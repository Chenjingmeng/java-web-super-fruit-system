package Fruit.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class LoginFilter implements Filter {
    public void destroy() {
    }
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        if ("/SuperFruitWeb_war_exploded/".equals(req.getRequestURI())){
            chain.doFilter(request,response);
            return;
        }
        // 判断访问资源路径是否和登录注册相关
        String[] urls = {"/errpage.jsp","/index.jsp","/images/","/css/","/js/","/LoginServlet","/zhuce.jsp","/ZhuCeServlet","/CheckServlet"};
        // 获取当前访问的资源路径
        String url = req.getRequestURL().toString();
        // 循环判断
        for (String u : urls) {
            if (url.contains(u)){
                // 找到了
                // 放行
                chain.doFilter(request, response);
                // 结束方法
                return;
            }
        }
        // 否则，不是上述路径
        // 1、判断loginServlet中的Session中是否存在
        HttpSession session = req.getSession();
        Object user = session.getAttribute("user");
        // 2、判断user是否为null
        if (user != null){
            // 已登录
            // 放行
            chain.doFilter(request, response);
        }else {
            // 没有登录，过滤器拦截，存储提示信息，跳转到登录页面
            req.setAttribute("msg","请先登录！");
            req.getRequestDispatcher("errpage.jsp").forward(req,response);
        }
    }
    public void init(FilterConfig filterConfig) throws ServletException {

    }

}
