package Fruit.dao;

import Fruit.tools.JDBCUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
    public String login(String name,String password)
    {
        Connection conn=null;
        ResultSet rs=null;
        PreparedStatement pstmt=null;
        String sql="select * from user where username=? and password=?";
        try {
            conn = JDBCUtils.getConnect();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.setString(2, password);
            rs = pstmt.executeQuery();
            if(rs.next()){
                return "1";
            }else {return "2";}
        }catch (ClassNotFoundException e)
        {e.printStackTrace();
        return "3";}
        catch (SQLException throwables){
            throwables.printStackTrace();
            return "4";
        }
        finally {
            JDBCUtils.release(conn,pstmt,rs);
        }
    }
    public String XiuGaiMiMa(String name,String newpass,String renewpass) {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sql = " update user set password=? where username=?";
        try {
            conn = JDBCUtils.getConnect();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, newpass);
            pstmt.setString(2, name);
            int num = pstmt.executeUpdate();
            if (num == 1&&newpass.equals(renewpass)) {
                return "1";
            } else {
                return "2";
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return "3";
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return "4";
        } finally {
            JDBCUtils.release(conn, pstmt);
        }
    }
    //检查用户密码是否正确
    public boolean XiuGai(String name,String password){
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs=null;
        try{
            con = JDBCUtils.getConnect();
            String sqlQuery = " select * from user where username=? and password=? ";
            pstmt= con.prepareStatement(sqlQuery);
            pstmt.setString(1, name);
            pstmt.setString(2,password);
            rs = pstmt.executeQuery();
            if(rs.next()){
                return true;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
