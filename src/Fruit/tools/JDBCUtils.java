package Fruit.tools;

import java.sql.*;

public class JDBCUtils {
    //加载驱动，并建立数据库连接
public static Connection getConnect()throws SQLException,ClassNotFoundException
{
    //1.注册数据库的驱动程序
    Class.forName("com.mysql.cj.jdbc.Driver");
    //2.通过DriverManager获取数据库连接
    String url="jdbc:mysql://localhost:3306/fruit?serverTimezone=GMT%2B8&useSSL=false";
    String username="root";
    String password="123456";
    Connection conn= DriverManager.getConnection(url,username,password);
    //3.通过Connection 对象获取Statement对象
//    stmt=conn.createStatement();
//    //4.使用Statement执行sql语句
//    String sql="select * from users";
//    rs=stmt.executeQuery(sql);
    return conn;
}
//进行操作后资源释放
public static void release(Connection conn, Statement stmt)
{
    if (stmt!=null)
    {
        try {
            stmt.close();}catch (SQLException e)
        {
            e.printStackTrace();
        }
        stmt=null;
    }
    if (conn!=null)
    {
        try {
            conn.close();}
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        conn=null;
    }
}
     public static void release(Connection conn, Statement stmt, ResultSet rs)
    {
        if (rs!=null)
        {
            try {
                conn.close();}
            catch (SQLException e)
            {
                e.printStackTrace();
            }
            rs=null;
        }
        release(conn,stmt);
    }
}
