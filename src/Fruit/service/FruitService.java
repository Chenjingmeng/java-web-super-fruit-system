package Fruit.service;



import Fruit.beans.Fruit;
import Fruit.beans.User;
import Fruit.dao.FruitDao;
import Fruit.dao.UserDao;

import java.util.ArrayList;

public class FruitService {
    private FruitDao dao=new FruitDao();

    public ArrayList<Fruit> queryAllFruit(){
        ArrayList<Fruit> fruits=dao.queryAllFruit();
        return fruits;
    }

    public ArrayList<Fruit> queryFruitByCond(Fruit fruit){
        ArrayList<Fruit> fruits=dao.queryFruitByCond(fruit);
        return fruits;
    }
//排序
    public ArrayList<Fruit> sortfruits(String sort){
        ArrayList<Fruit> fruits=dao.sortFruit(sort);
        return fruits;
    }
    //降序
    public ArrayList<Fruit> Jiangsortfruits(String sort){
        ArrayList<Fruit> fruits=dao.Jiangsortfruits(sort);
        return fruits;
    }
    public boolean addFruit(Fruit fruit){
        ArrayList<Fruit> fruits=queryAllFruit();
        for(Fruit data:fruits){
            if(fruit.getNumber().equals(data.getNumber()))
                return false;
        }
        dao.addFruit(fruit);
        return true;
    }

    public boolean updateFruit(Fruit fruit){
        ArrayList<Fruit> fruits=queryAllFruit();
        for(Fruit data:fruits){
            if(fruit.getNumber().equals(data.getNumber())){
                dao.updateFruit(fruit);
                return true;
            }
        }
        return false;
    }

    public boolean delFruit(String delNumber){
        ArrayList<Fruit> fruits=queryAllFruit();
        for(Fruit data:fruits){
            if(delNumber.equals(data.getNumber())){
                dao.delFruit(delNumber);
                return true;
            }
        }
        return false;
    }
    public String XiuGai(String name,String pass,String newpass,String renewpass){
        UserDao dao = new UserDao();
        if(dao.XiuGai(name,pass)){
            if(pass!=null&&pass.length()>0){
                return dao.XiuGaiMiMa(name,newpass,renewpass);
            }else {
                return "6";
            }
        }
        return "5";
    }

}
